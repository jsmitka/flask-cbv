============
Installation
============

At the command line::

    $ easy_install flask-cbv

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv flask-cbv
    $ pip install flask-cbv